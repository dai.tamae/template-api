const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid, v4 } = require("uuid");
const decode = require("jsonwebtoken/decode");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));
    
  api.post("/users",(req,res)=>{

    let user=req.body;
    
    if(!(user.name)){
      res.status(400);
      res.send({
        "error": "Request body had missing field name"
      });
      return;
    }
    if(!(user.email)){
      res.status(400);
      res.send({
        "error": "Request body had missing field email"
      });
      return;
    }
    if(!(user.password)){
      res.status(400);
      res.send({
        "error": "Request body had missing field password"
      });
      return;
    }
    if(!(user.passwordConfirmation)){
      res.status(400);
      res.send({
        "error": "Request body had missing field passwordConfirmation"
      });
      return;
    }

    if(!(typeof user.name === 'string' 
      || user.name instanceof String)
      || user.name.length==0
      ){
      res.status(400);
      res.send({
        "error": "Request body had malformed field name",
      });
      return;
      
    }
    let re = new RegExp("^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?");
    if(!re.test(user.email)
    ){
      res.status(400);
      res.send({
        "error": "Request body had malformed field email"
      });
      return;
    }

    if(!(typeof user.password === 'string' 
    || user.password instanceof String)
    || user.password.length<8
    || user.password.length>32
    ){
      res.status(400);
      res.send({
        "error": "Request body had malformed field password"
      });
      return;
    }
    if(!(typeof user.passwordConfirmation === 'string' 
    || user.passwordConfirmation instanceof String)
    || user.passwordConfirmation.length<8
    || user.passwordConfirmation.length>32
    ){
      res.status(400);
      res.send({
        "error": "Request body had malformed field passwordConfirmation"
      });
      return;
    }

    if(user.password != user.passwordConfirmation){
      res.status(422);
      res.send({
        "error": "Password confirmation did not match",
      });
      return;
    }

    let id=uuid();
    
    const exists=mongoClient.db().collection().findOne();

    if(exists){
      res.status(400);
      res.send({
        "error": "The selected email is already registered",
      });
      return;
    }

    res.status(201);

    res.send(
      {
        "user": {
          "id": id,
          "name": user.name,
          "email": user.email
        }
      }
      
    );
    
    stanConn.publish('users',
      JSON.stringify({
        "eventType": "UserCreated",
        "entityId": id,
        "entityAggregate": {
          "name": user.name,
          "email": user.email,
          "password": user.password
        }
      })); 

    return;
  });
  
  api.delete("/users/:uuid",(req,res)=>
  {
    let token=req.get("Authentication");
    if(!token){
      res.status(400);
      res.send(
        {
          "error": "Access Token not found",
        }
      );
      return;
    }
    token=token.split(" ")[1];
    let decoded=jwt.decode(token);
    if(decoded.id!=req.params.uuid){
      res.status(403);
      res.send({
        "error": "Access Token did not match User ID",
      });
      return;
    }
    
    stanConn.publish('users',
      JSON.stringify( {
        "eventType": "UserDeleted",
        "entityId": req.params.id,
        "entityAggregate": {}
      }
    ));
    
    res.status(200);
    res.send(
      {
        "id": req.params.uuid
      }
    );

  });

  return api;
};
